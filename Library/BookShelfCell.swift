//
//  BookShelfCell.swift
//  Library
//
//  Created by Stefania Zinno on 09/12/2019.
//  Copyright © 2019 Stefania Zinno. All rights reserved.
//

import UIKit

class BookShelfCell: UITableViewCell {
    
    @IBOutlet var cover: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var author: UILabel!
    @IBOutlet var genre: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
