//
//  AddBook.swift
//  Library
//
//  Created by Stefania Zinno on 10/12/2019.
//  Copyright © 2019 Stefania Zinno. All rights reserved.
//

import UIKit

class AddBook: UITableViewController, UINavigationControllerDelegate{
    
    override func viewDidLoad() {super.viewDidLoad()}
    
    @IBOutlet var infoBookToAdd: [UITextField]!
    @IBAction func addBookToLibrary(_ sender: Any) {
        CKManager.shared.saveRecord(outletCollection: infoBookToAdd, coverToSave: imageTake.image){ (result) in
            DispatchQueue.main.async {
                self.dismiss(animated: true) {}
            }
            
        }
    }
    
    //MARK: - Photo Capturing Infos
    @IBOutlet weak var imageTake: UIImageView!
    var imagePicker: UIImagePickerController!
    
    //MARK: - Take image
    @IBAction func takePhoto(_ sender: UIButton) {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self        
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


extension AddBook :  UIImagePickerControllerDelegate  {
    
    //MARK: - Done image capture here
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }        
        imageTake.image = selectedImage
        
    }
}
