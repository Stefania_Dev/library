//
//  CKManager.swift
//  Library
//
//  Created by Stefania Zinno on 09/12/2019.
//  Copyright © 2019 Stefania Zinno. All rights reserved.
//

import UIKit
import CloudKit

class CKManager {    
    static let shared = CKManager()
    let publicDatabase = CKContainer.default().publicCloudDatabase
    var reader = CKRecord(recordType: "Reader")
    
    //    MARK: fetchRecords
    func fetchRecords (completion: @escaping ( Result<[CKRecord], Error>) -> Void){
        var retrieved: [CKRecord] = []
        let allPredicate = NSPredicate(value: true)
        let query = CKQuery(recordType: "Book", predicate: allPredicate)
        query.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        let operation = CKQueryOperation(query: query)
        operation.desiredKeys = ["Author","Genre","Title","Cover"]
        operation.resultsLimit = 50
        publicDatabase.add(operation)        
        operation.recordFetchedBlock = { record in
            retrieved.append(record)
            completion(.success(retrieved))
        }
        
    }
    //    MARK: deleteRecord
    func deleteRecord (recrdoToDelete: CKRecord, completion: @escaping ( Result<CKRecord.ID, Error>) -> Void){
        publicDatabase.delete(withRecordID: recrdoToDelete.recordID) { (recordId, error) in
            if let recordId = recordId {completion(.success(recordId))}
        }
        
    }
    //    MARK: saveRecord
    func saveRecord (outletCollection: [UITextField], coverToSave: UIImage?, completion: @escaping (Result<CKRecord, Error>) -> Void){
        
        
        
        let record = CKRecord(recordType: "Book")
        let reference = CKRecord.Reference(recordID: reader.recordID, action: .deleteSelf)
        record["ReaderThatReadsTheBook"] = reference
        record["Title"] = outletCollection.first?.text
        record["Genre"] = outletCollection[2].text
        record["Author"] = outletCollection[1].text
        
        // Create a URL in the /tmp directory
        guard let imageURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("TempImage.png") else {
            return
        }
        // save image to URL
        do { try coverToSave!.pngData()?.write(to: imageURL)
        } catch { print("there was an error \(error) ")}
        let imageAsset = CKAsset(fileURL: imageURL)
        record["Cover"] = imageAsset
        
        publicDatabase.save(record) { (record, error) in
            if let error = error {
                completion(.failure(error))
            }
            if let record = record {
                completion(.success(record))
            }
        }
    }
    
    //    MARK: modifiyRecord
    func modifiyRecord (rowRecord: CKRecord, completion: @escaping (Result<CKRecord, Error>) -> Void){
        publicDatabase.fetch(withRecordID: rowRecord.recordID) { (record, error) in
            
        }
    }
    
    //    MARK: fetchReader
    func fetchReader (completion: @escaping ( Result<[CKRecord], Error>) -> Void){
        let allPredicate = NSPredicate(value: true)
        let query = CKQuery(recordType: "Reader", predicate: allPredicate)
        publicDatabase.perform(query, inZoneWith: nil) { (record, error) in
            if let record = record {
                self.reader = record.first!
            }
        }
    }
    
    //MARK: - Subscriptions
    func subscribeToAddBook (){
        let predicate = NSPredicate(value: true)
        let subscription = CKQuerySubscription(recordType: "Book", predicate: predicate, options: .firesOnRecordCreation)
        
        let notification = CKSubscription.NotificationInfo()
        notification.alertBody = "There's a new book in the library."
        notification.soundName = "default"

        subscription.notificationInfo = notification
        
        publicDatabase.save(subscription) { result, error in
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
}



